//Setup the dependencies
const express = require('express')
const mongoose = require('mongoose')
//This allow us to use all the routes defined in the routes
const taskRoute = require('./routes/taskRoute')

//Server setup
const app = express()
const port = 3001
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.vlaci.mongodb.net/batch127_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//Connection error handling
let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"))
db.once("open", ()=>console.log("We're now connected to the cloud database"))

//Add the task route
//Allows all the task routes created in the "taskRoute.js" file to use the "tasks" route
app.use('/tasks',taskRoute)

app.listen(port, ()=>console.log(`
=============================================================
   _           _     
  (_) ___  ___| |__  
  | |/ _ \\/ __| '_ \\ 
  | | (_) \\__ \| | | |
 _/ |\\___/|__/|_| |_| is now listening to ${port}
|__/
=============================================================
`))