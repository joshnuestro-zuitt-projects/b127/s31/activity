//Controllers contain the functions and business logic of our Express js application
//Meaning all the operations it can do will be placed in this file

//Allows us to use the contents of the "task.js" file in the models folder
const Task = require('../models/task') //no .js needed

//Controller function for getting all the tasks
module.exports.getAllTasks = ()=>{
	
	//Returns the result of the Mongoose methond "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" route is accessed
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client
	return Task.find({}).then(result => {
		//Returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
}

//Controller function for creating task
//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed "requestBody" parameter in the controller file
module.exports.createTask = (requestBody)=>{
	//Create a task object based on the Mongoose model Task
	let newTask = new Task({
		name: requestBody.name
	})

	//Saves the newly created "newTask" object in the MongoDB database
	//.then method waits until the task is stored/error
	//.then method accepts 2 arguments
		//Firt paramater restore the result returned by the save method
		//Second parameter stores the error object
	//Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter
	return newTask.save().then((task,error)=>{
		//If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
		//Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other form executing code
		if(error){
			console.log(error)
			return false
		}
		else{
			return task
		}
	})
}

//Delete task
//Business Logic
/*
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId)=>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err)
			return false
		}
		else{
			//Delete successful, returns the removed tak object back to the client
			return `This object has been deleted:\n${removedTask}`
		}
	})
}

//Updating a task
//Business Logic
/*
1. Get the task with the id using the method "findById"
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task
*/

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false;
		}

		//Results of the "findById" method will be stored in the "result" parameter
		//Its "name" property will be reassigned the value of the "name" received from the request body
		result.name = newContent.name

		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}
			else{
				return updatedTask
			}
		})
	})
}

//4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
module.exports.getTask = (taskId)=>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false
		}
		else{
		return result
		}
	})
}

//8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false;
		}
		result.status = newContent.status

		return result.save().then((updatedStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}
			else{
				return updatedStatus
			}
		})
	})
}

