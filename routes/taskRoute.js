//Route => contains all the endpoints for our application
//We separate the routes such that "index.js" only contains information on the server
//We need to use express Router() function to achieve this
const express = require('express')
//Creates a Router instance that functions as a middleware and routing system
//Allows access to HTTP method middlewares that makes it easier to creaete routes for our application
const router = express.Router()
//The "taskController" allows us to use the functions defined in the taskController.js file
const taskController = require('../controllers/taskController') //no .js needed

//Routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a routes is accessed

router.get('/getAll',(req,res)=>{
	//Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client

	//"resultFromController" is only used here to make the code easier to understand but it's comon practice to use the shorthand parameter name for a result using the parameter name "result/res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//Create new Task
router.post('/',(req,res)=>{
	//The createTask function needs the data from the request body, so we need to supply it to the function
	//If information is coming from the client, commonly from forms, the data can be accessed from the request "body"
	taskController.createTask(req.body).then(result=>res.send(result))
})

//to delete a task
//This route expects to receive a DELETE request at the URL "/tasks/:id"
//The task ID is obtained from the URL is denoted by the ":id" identifier in the route
//THe colon (:) is an identifier that helps us create a dynamic route which allows us to supply information in the URL
//":id" is a WILDCARD where you can put any vale, it then creates a link between "id" parameter in the URL and the value provided in the URL
//ex
	//If the route is localhost:3000/task/1234
	//1234 is assigned to the "id" parameter in the URL
router.delete('/:id',(req,res)=>{
	//If the information comes from the URL, the data can be accessed from the request "params" property
	//In this case "id" is the name of the parameter, the property name of this object will match the given URL parameter
	//req.params.id
	taskController.deleteTask(req.params.id).then(result=>res.send(result))
})

//Update a task
router.put('/:id',(req,res)=>{
		//req.params.id retrieves the task id from the parameter
		//req.body retrieves the data of the updates that will be applied to a task from the request body
	taskController.updateTask(req.params.id, req.body).then(result=>res.send(result))
})

////4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
router.get('/:id',(req,res)=>{
	taskController.getTask(req.params.id, req.body).then(result=>res.send(result))
})

//8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
router.put('/:id/complete',(req,res)=>{
	taskController.updateStatus(req.params.id, req.body).then(result=>res.send(result))
})

module.exports = router
